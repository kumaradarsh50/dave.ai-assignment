const button = document.getElementById('readfile');

const arrayOne = [];
const arrayTwo = [];
const arrayThree = [];
const oldData = [];
const newData = [];
const mySets = new Set();

function readFileOne(input) {
  let file = input.files[0];
  let reader = new FileReader();
  reader.readAsText(file);
  reader.onload = function () {
    let csvDataOne = reader.result;
    const lines = csvDataOne.split('\n');
    let headers = lines[0].split(',');

    for (let i = 1; i < lines.length; i++) {
      let rowData = lines[i].split(',');
      arrayOne[i] = {};

      for (let j = 0; j < rowData.length; j++) {
        arrayOne[i][headers[j]] = rowData[j];
      }
    }

    // console.log(arrayOne);
  };
  reader.onerror = function () {
    console.log(reader.error);
  };
}

function readFileTwo(input) {
  let file = input.files[0];
  let reader = new FileReader();
  reader.readAsText(file);
  reader.onload = function () {
    let csvDataTwo = reader.result;
    const lines = csvDataTwo.split('\n');
    let headers = lines[0].split(',');

    for (let i = 1; i < lines.length; i++) {
      let rowData = lines[i].split(',');
      arrayTwo[i] = {};

      for (let j = 0; j < rowData.length; j++) {
        arrayTwo[i][headers[j]] = rowData[j];
      }
    }
  };
  reader.onerror = function () {
    console.log(reader.error);
  };
}

button.addEventListener('click', () => {
  for (const arr of arrayOne) {
    arrayThree.push(arr);
  }
  for (const arr of arrayTwo) {
    arrayThree.push(arr);
  }
  filterArray();
  uniqArray();
  // console.log(newData, oldData);
  oldTable(oldData);
  newTable(newData);
});

function uniqArray() {
  let ar = [
    ...oldData
      .reduce((map, obj) => map.set(obj.mobile_number, obj), new Map())
      .values(),
  ];
  newData.push(ar);
}

function filterArray() {
  const array = arrayThree.filter((el) => {
    return el !== undefined;
  });
  for (const ar of array) {
    if (ar.first_name !== '') {
      oldData.push(ar);
    }
  }
}

// create table
function oldTable(data) {
  console.log(data);
  let oldTable = document.getElementById('olddatatable');
  for (let i = 0; i < data.length; i++) {
    let row = `<tr>
                <td>${data[i].first_name}</td>
                <td>${data[i].last_name}</td>
                <td>${data[i].company_name}</td>
                <td>${data[i].address}</td>
                <td>${data[i].city}</td>
                <td>${data[i].county}</td>
                <td>${data[i].state}</td>
                <td>${data[i].zip}</td>
                <td>${data[i].mobile_number}</td>
                <td>${data[i].email}</td>
                <td>${data[i].web}</td>
            </tr>`;
    oldTable.innerHTML += row;
  }
}

function newTable(data) {
  let newTable = document.getElementById('newdatatable');
  const dat = data.flat();
  console.log(dat);
  for (let i = 0; i < dat.length; i++) {
    let row = `<tr>
                <td>${dat[i].first_name}</td>
                <td>${dat[i].last_name}</td>
                <td>${dat[i].company_name}</td>
                <td>${dat[i].address}</td>
                <td>${dat[i].city}</td>
                <td>${dat[i].county}</td>
                <td>${dat[i].state}</td>
                <td>${dat[i].zip}</td>
                <td>${dat[i].mobile_number}</td>
                <td>${dat[i].email}</td>
                <td>${dat[i].web}</td>
            </tr>`;
    newTable.innerHTML += row;
  }
}
